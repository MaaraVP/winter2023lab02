import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter three values");
		int value1 = scan.nextInt();
		int value2 = scan.nextInt();
		int value3 = scan.nextInt();
		
		AreaComputations ac = new AreaComputations();
		System.out.println(ac.areaSquare(value1));
		System.out.println(ac.areaRectangle(value2, value3));
	}
}